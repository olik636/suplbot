package commands

import (
	"database/sql"
	"fmt"

	"github.com/go-telegram-bot-api/telegram-bot-api"

	"bitbucket.org/olik636/suplbot/pkg/database"
	"bitbucket.org/olik636/suplbot/pkg/structs"
	"bitbucket.org/olik636/suplbot/pkg/suplovani"
	"github.com/Sirupsen/logrus"
)

//Context ... command context
type Context struct {
	Supl   chan suplovani.SuplRequest
	CMDs   chan uint
	Dbh    *sql.DB
	Log    *logrus.Logger
	Update *tgbotapi.Update
	Bot    *tgbotapi.BotAPI
}

//Dnes ... today
func Dnes(c *Context) {
	bc := make(chan suplovani.Suplovani)
	class, err := database.GetClass(c.Dbh, int(c.Update.Message.Chat.ID))
	if err != nil {
		c.Log.WithError(err).Errorf("Can't get data from db!")
	}
	c.Supl <- suplovani.SuplRequest{Chan: bc, Class: structs.ClassString(class),
		ChatID: int(c.Update.Message.Chat.ID), MessageID: int(c.Update.Message.MessageID), Day: suplovani.TODAY}
	supl := <-bc
	day, wd := structs.GenerateDay(supl.TodayDay, c.Log)
	if wd {
		send(c, fmt.Sprintf("Dnes je %s...", day))
	}
	if class == 0 {
		send(c, fmt.Sprintf("Suplování na %s:\n[https://is.jaroska.cz](%s)\nNemáš nastavenou svou třídu...",
			day, supl.TodayURL))
		return
	}
	o, ok := supl.Today[structs.ClassString(class)]
	if !ok {
		send(c, fmt.Sprintf("Na %s není žádné %s [suplování](%s)...\n",
			structs.ClassString(class), day, supl.TodayURL))
		return
	}
	msg := "[Suplování](" +
		supl.TodayURL + ") na " + day + " pro " + structs.ClassString(class) + ":\n"
	msg = suplovani.GenerateCombinedSupl(msg, o)
	send(c, msg)
}

//Zitra ... tomorrow
func Zitra(c *Context) {
	bc := make(chan suplovani.Suplovani)
	class, err := database.GetClass(c.Dbh, int(c.Update.Message.Chat.ID))
	if err != nil {
		c.Log.WithError(err).Errorf("Can't get data from db!")
	}
	c.Supl <- suplovani.SuplRequest{Chan: bc, Class: structs.ClassString(class),
		ChatID: int(c.Update.Message.Chat.ID), MessageID: int(c.Update.Message.MessageID), Day: suplovani.TOMORROW}
	supl := <-bc
	day, _ := structs.GenerateDay(supl.TomorrowDay, c.Log)
	if !supl.IsTomorrow {
		send(c, "Na zítra ještě nebylo vystaveno suplování")
		return
	}
	_, ok := supl.Tomorrow[structs.ClassString(class)]
	if class == 0 {
		send(c, fmt.Sprintf("[Suplování](%s) na %s\nNemáš nastavenou svou třídu...",
			supl.TomorrowURL, day))
		return
	}
	if !ok {
		send(c, fmt.Sprintf("Na %s není žádné %s [suplování](%s)...\n",
			structs.ClassString(class), day, supl.TomorrowURL))
		return
	}
	msg := "[Suplování](" + supl.TomorrowURL + ") na " + day + " pro " + structs.ClassString(class) + ":\n"
	msg = suplovani.GenerateCombinedSupl(msg, supl.Tomorrow[structs.ClassString(class)])
	send(c, msg)
}

func send(c *Context, rmsg string) {
	msg := tgbotapi.NewMessage(c.Update.Message.Chat.ID, rmsg)
	msg.ReplyToMessageID = c.Update.Message.MessageID
	msg.ParseMode = "markdown"
	_, err := c.Bot.Send(msg)
	if err != nil {
		c.Log.WithError(err).Errorf("Can't send message")
	}
}
