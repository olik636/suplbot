package commands

import (
	"bitbucket.org/olik636/suplbot/pkg/database"
)

//ToggleNotifications ... set if user want be notified
func ToggleNotifications(c *Context) {
	tgn, err := database.ToggleNotify(c.Dbh, int(c.Update.Message.Chat.ID))
	if err != nil {
		send(c, "Někde se stala chyba...")
		c.Log.WithError(err).Error("Can't toggle notifications for user")
		return
	}
	send(c, generateNotifyResp(tgn))
}

func generateNotifyResp(a bool) string {
	if a == true {
		return "Notifikace zapnuty"
	}
	return "notifikace vypnuty"
}
