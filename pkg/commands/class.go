package commands

import (
	"bitbucket.org/olik636/suplbot/pkg/database"
	"bitbucket.org/olik636/suplbot/pkg/structs"
)

//SetClass ... set user's class
func SetClass(c *Context) {
	class := structs.ParseClass(c.Update.Message.CommandArguments())
	if class == nil {
		send(c, "Prosím zadávej ve tvaru `/trida 1.A` nebo `/trida 4.ag`")
		return
	}
	err := database.SetClass(c.Dbh, int(c.Update.Message.Chat.ID), *class)
	if err != nil {
		c.Log.WithError(err).Errorf("Can't set user's class")
		send(c, "Něco se nepovedlo, prosím vydrž, na problému se pracuje...")
	}
	send(c, "Nastaveno!")

}
