package commands

import (
	"fmt"

	"github.com/go-telegram-bot-api/telegram-bot-api"

	"bitbucket.org/olik636/suplbot/pkg/database"
	"bitbucket.org/olik636/suplbot/pkg/suplovani"
)

//ToggleHoliday ... toggle holiday
func ToggleHoliday(c *Context) {
	bc := make(chan suplovani.Suplovani)
	c.CMDs <- suplovani.CMDHolidayToggle
	c.Supl <- suplovani.SuplRequest{Chan: bc}
	supl := <-bc
	send(c, fmt.Sprintf("Holiday set to: %t", supl.IsHoliday))
}

//PreviousID ... id--
func PreviousID(c *Context) {
	bc := make(chan suplovani.Suplovani)
	c.CMDs <- suplovani.CMDPreviusID
	c.Supl <- suplovani.SuplRequest{Chan: bc}
	supl := <-bc
	send(c, fmt.Sprintf("SuplID: %d", supl.ID))
}

//NextID ... id++
func NextID(c *Context) {
	bc := make(chan suplovani.Suplovani)
	c.CMDs <- suplovani.CMDNextID
	c.Supl <- suplovani.SuplRequest{Chan: bc}
	supl := <-bc
	send(c, fmt.Sprintf("SuplID: %d", supl.ID))
}

//Broadcast ... sends message to all users
func Broadcast(c *Context) {
	defer recover()
	ids, err := database.SelectAllChatID(c.Dbh)
	if err != nil {
		c.Log.WithError(err).Error("Can't select all users")
	}
	pmsg := tgbotapi.NewMessage(0, c.Update.Message.CommandArguments())
	pmsg.ParseMode = "markdown"
	c.Log.WithField("user", c.Update.Message.From.String()).Infof("Broadcasting message: '%s'", pmsg.Text)
	for _, v := range ids {
		pmsg.ChatID = int64(v)
		c.Bot.Send(pmsg)
	}
}

//List ... list all users
func List(c *Context) {
	defer recover()
	ids, err := database.SelectAllNames(c.Dbh)
	if err != nil {
		c.Log.WithError(err).Error("Can't select all users")
	}
	msg := "*Users:*\n"
	for _, v := range ids {
		msg += v + "\n"
	}
	send(c, msg)
}
