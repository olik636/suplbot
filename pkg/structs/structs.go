package structs

import (
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
)

// SuplItem ... one line in supl
type SuplItem struct {
	Hour          string
	Missing       string
	Subject       string
	Classroom     string
	NewClassroom  string
	SupplyTeacher string
	Type          string
	Note          string
}

//ParseClass ... parse class
func ParseClass(sc string) *int {
	var class int
	sc = strings.TrimSpace(sc)
	r := regexp.MustCompile(`^[1-4]\.([a,b]g|[A-D])$`)
	if !r.Match([]byte(sc)) {
		return nil
	}
	grade, _ := strconv.Atoi(string(sc[0]))
	if len(sc) == 4 {
		class = 2 * grade
		if string(sc[2]) == "a" {
			class--
		}
	} else {
		class = 4 * grade
		switch string(sc[2]) {
		case "A":
			class -= 3
		case "B":
			class -= 2
		case "C":
			class--
		}
		class += 8
	}
	return &class
}

//ClassString ... stringify class number
func ClassString(c int) string {
	var class string
	if c < 9 {
		g := c / 2
		switch c % 2 {
		case 0:
			class += "ag"
		case 1:
			class += "bg"
		}
		class = strconv.Itoa(g+1) + "." + class
	} else {
		c -= 9
		g := c / 4
		class = strconv.Itoa(g+1) + "."
		switch c % 4 {
		case 0:
			class += "A"
		case 1:
			class += "B"
		case 2:
			class += "C"
		case 3:
			class += "D"
		}
	}
	return class
}

//GenerateDay ... generate czech name of weekday
func GenerateDay(d time.Weekday, log *logrus.Logger) (string, bool) {
	switch d {
	case time.Monday:
		return "pondělí", false
	case time.Tuesday:
		return "úterý", false
	case time.Wednesday:
		return "středa", false
	case time.Thursday:
		return "čtvrtek", false
	case time.Friday:
		return "pátek", false
	case time.Saturday:
		return "sobota", true
	case time.Sunday:
		return "neděle", true
	}
	log.WithField("day", d).Error("Entered weekday is not weekday?")
	return "", false

}
