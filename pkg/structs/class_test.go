package structs

import "testing"

var data = map[string]int{
	"1.ag": 1,
	"1.A":  9,
	"1.B":  10,
	"2.B":  14,
	"1.a":  -1,
	"4.ag": 7,
	"9.A":  -1,
}

func TestParseClass(t *testing.T) {
	var m *int
	for i, v := range data {
		m = &v
		if v == -1 {
			m = nil
		}
		r := ParseClass(i)
		if (m == nil && r != m) || (r != nil && m != nil && *m != *r) {
			t.Errorf("Expected: %d, got %d", *m, *r)
		}
	}
}
