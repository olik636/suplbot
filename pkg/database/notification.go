package database

import (
	"database/sql"
	"strconv"
)

//SelectAllWithNotification ... Select all users with notification set to true
func SelectAllWithNotification(db *sql.DB) ([]int64, error) {
	var users []int64
	var user string
	row, err := db.Query("SELECT chat_id FROM users WHERE notify=1")
	for row.Next() {
		row.Scan(&user)
		id, err := strconv.Atoi(user)
		if err != nil {
			return nil, err
		}
		users = append(users, int64(id))
	}
	return users, err
}

//ToggleNotify ... Toggle notifications for users
func ToggleNotify(db *sql.DB, chatID int) (bool, error) {
	var notify bool
	err := db.QueryRow("SELECT notify FROM users WHERE chat_id=?", chatID).Scan(&notify)
	if err != nil {
		return false, err
	}
	_, err = db.Exec("UPDATE users SET notify=? WHERE chat_id=?", !notify, chatID)
	return !notify, err
}
