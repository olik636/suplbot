package database

import (
	"database/sql"
	"strconv"
)

//SelectAllNames ... selects all names
func SelectAllNames(db *sql.DB) ([]string, error) {
	var users []string
	var user string
	row, err := db.Query("SELECT name FROM users")
	for row.Next() {
		row.Scan(&user)
		users = append(users, user)
	}
	return users, err
}

//SelectAllChatID ... selects all chat ids
func SelectAllChatID(db *sql.DB) ([]int, error) {
	var users []int
	var user string

	row, err := db.Query("SELECT chat_id FROM users")
	for row.Next() {
		row.Scan(&user)
		id, err := strconv.Atoi(user)
		if err != nil {
			return nil, err
		}
		users = append(users, id)
	}
	return users, err
}

//RegisterUser ... registers user to database
func RegisterUser(db *sql.DB, chatID int, name string) (bool, error) {
	var answ bool
	db.QueryRow("SELECT COUNT(*) FROM users WHERE chat_id=?", chatID).Scan(&answ)
	if answ {
		return true, nil
	}
	_, err := db.Exec("INSERT INTO users (name,chat_id,dt_registration) values (?,?,datetime('now'))", name, chatID)
	return false, err
}

//SetClass ... set users class
func SetClass(db *sql.DB, chatID int, class int) error {
	_, err := db.Exec("UPDATE users SET class=? WHERE chat_id=?", class, chatID)
	return err
}

func GetClass(db *sql.DB, chatID int) (int, error) {
	var answ int
	err := db.QueryRow("SELECT class FROM users WHERE chat_id=?", chatID).Scan(&answ)
	return answ, err
}
