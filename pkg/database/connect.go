package database

import "database/sql"

//Connect ... connects to database
func Connect(file string) (*sql.DB, error) {

	db, err := sql.Open("sqlite3", file)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}
	return db, nil
}
