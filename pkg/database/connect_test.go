package database

import (
	"io/ioutil"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

func TestConnectSuccess(t *testing.T) {
	tmp, err := ioutil.TempFile("", "test-db")
	if err != nil {
		t.Fatalf("Can't create temporary db file: %s", err.Error())
	}
	defer os.Remove(tmp.Name())
	db, err := Connect(tmp.Name())

	if err != nil {
		t.Errorf("Unexpected error during connect: %s", err.Error())
	} else {
		db.Close()
	}
}

func TestConnectUnaccessibleFile(t *testing.T) {
	expectedError := "unable to open database file"

	tmp, err := ioutil.TempFile("", "test-db2")
	if err != nil {
		t.Fatalf("Can't create temporary db file: %s", err.Error())
	}
	os.Chmod(tmp.Name(), 0000)
	defer os.Remove(tmp.Name())
	db, err := Connect(tmp.Name())

	switch {
	case err == nil:
		t.Error("Expected error, but call finished successfully")
	case err.Error() != expectedError:
		t.Errorf("Expected error '%s', but got '%s", expectedError, err.Error())
	}

	if db != nil {
		db.Close()
	}
}
