package parse

import (
	"io"
	"strconv"
	"strings"

	"bitbucket.org/olik636/suplbot/pkg/structs"
	"golang.org/x/net/html"
)

type rowSpan struct {
	Class string
	Rows  int
}

const PRE_TR = 1
const TABLE = 2

// HTML ... parse html
func HTML(plainHTML string) (map[string][]structs.SuplItem, error) {
	z := html.NewTokenizer(strings.NewReader(plainHTML))

	var table uint
	var row uint
	var class string
	var col uint
	var rowSpan = rowSpan{}
	var tr bool
	var err error
	var item structs.SuplItem
	var td bool
	var supl = make(map[string][]structs.SuplItem)
	var firstSpan bool

	for {
		tt := z.Next()
		tn, _ := z.TagName()
		switch tt {
		case html.ErrorToken:
			err := z.Err()
			if err == io.EOF {
				return supl, nil
			}
			return nil, err
		case html.StartTagToken:
			if string(tn) == "table" {
				table++
				if table > TABLE {
					return supl, nil
				}
			}
			if table == TABLE && row > PRE_TR && string(tn) == "td" {
				td = true
				for key, val, more := z.TagAttr(); more; key, val, more = z.TagAttr() {
					if string(key) == "rowspan" {
						firstSpan = true
						rowSpan.Class = ""
						rowSpan.Rows, err = strconv.Atoi(string(val))
						if err != nil {
							return nil, err
						}
						break
					}
				}
			} else if string(tn) == "tr" {
				tr = true
			}
		case html.TextToken:
			if table == TABLE && row > PRE_TR && tr && td {
				if rowSpan.Rows < 1 {
					class = setSupl(string(z.Text()), class, col, &item)
				} else if rowSpan.Rows > 0 {
					if rowSpan.Class == "" {
						rowSpan.Class = string(z.Text())
					}
					if firstSpan {
						class = setSupl(string(z.Text()), rowSpan.Class, col, &item)
					} else {
						class = setSupl(string(z.Text()), rowSpan.Class, col+1, &item)
					}
				}
				col++
			}
		case html.EndTagToken:
			if string(tn) == "tr" && table == TABLE {
				tr = false
				rowSpan.Rows--
				col = 0
				if row > PRE_TR {
					supl = saveSuplItem(supl, item, class)
					item = structs.SuplItem{}
				}
				row++
				firstSpan = false
			} else if string(tn) == "td" && table == TABLE {
				td = false
			}
		}
	}
}

func saveSuplItem(supl map[string][]structs.SuplItem, si structs.SuplItem, class string) map[string][]structs.SuplItem {
	cls := strings.Split(class, ", ")
	for _, v := range cls {
		_, ok := supl[v]
		if !ok {
			supl[v] = []structs.SuplItem{}
		}
		supl[v] = append(supl[v], si)
	}
	return supl
}

func setSupl(txt string, class string, col uint, s *structs.SuplItem) string {
	txt = strings.TrimSpace(txt)
	switch col {
	case 0:
		return txt
	case 2:
		s.Hour = txt
	case 3:
		s.Missing = txt
	case 4:
		s.Subject = txt
	case 5:
		s.Classroom = txt
	case 6:
		s.NewClassroom = txt
	case 7:
		s.SupplyTeacher = txt
	case 8:
		s.Type = txt
	case 9:
		s.Note = txt
	}
	return class
}
