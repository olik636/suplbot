package parse

import "testing"

func TestHTMLParsing(t *testing.T) {
	supl, err := HTML(htmlTest)
	t.Errorf("%+v", supl["2.B"])
	if err != nil {
		t.Errorf("%s", err.Error())
	}
}

const htmlTest = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Zastupov�n�-p�tek  21. 9. 2018</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Refresh" content="180">
<STYLE TYPE="text/css"><!--
.StyleB0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleB1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleB2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,204)}

.StyleC0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleC1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleC2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC3 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC4 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC5 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC6 { border-style: solid;padding:0px; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleH2 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleZ0 { background-color: #DBE6EE }

.StyleZ1 { width: 700}

.StyleZ2 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: right; width: 700}

.StyleZ3 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ4 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ5 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: left}

.StyleZ6 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: right}

--></STYLE>
</HEAD>
<BODY CLASS="StyleZ0">
<DIV CLASS="StyleZ2">Gymn�zium Brno, t��da Kapit�na Jaro�e, p��sp�vkov� organizace</DIV>
<DIV CLASS="StyleZ3">p�tek  21. 9. 2018</DIV><BR>
<DIV CLASS="StyleZ4">Chyb�j�c�</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">Herman, Clausov�, Kallus Brychov�, Kobza, Liter�kov� (0..3), Ma��kov�, Pulkr�bek (4), Stupka, Zub��kov�</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">4bg</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Suplov�n�</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="98" HEIGHT="0"></TD>
<TD WIDTH="41" HEIGHT="0"></TD>
<TD WIDTH="38" HEIGHT="0"></TD>
<TD WIDTH="49" HEIGHT="0"></TD>
<TD WIDTH="47" HEIGHT="0"></TD>
<TD WIDTH="43" HEIGHT="0"></TD>
<TD WIDTH="90" HEIGHT="0"></TD>
<TD WIDTH="74" HEIGHT="0"></TD>
<TD WIDTH="25" HEIGHT="0"></TD>
<TD WIDTH="160" HEIGHT="0"></TD>
<TD WIDTH="35" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">T��da</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Budova</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Hodina</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Chyb�j�c�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">P�edm�t</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">U�ebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">N�hradn� u�ebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zastupuj�c�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Typ zastupov�n�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Pozn�mka pro vyu�uj�c�ho</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zna�ka</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">He</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kre</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=3 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lac</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">CvMn</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">7</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">CvMn</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hor</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S, D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hor</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S, D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.B, 2.C, 2.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pul</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Du3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Vv</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">La3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ju</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ple</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hnd</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=3 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Z</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mrk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mrk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=4 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">In</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mcv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mcv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.B, 4.C, 4.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">La3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.A, 4.B, 4.C, 4.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Stu</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sq</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">7</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Stu</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sq</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P1b</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pt</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P2b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sm�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3ag</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3a</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sta</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zadan� pr�ce do A</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3ag, 3bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3a</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ad</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">R</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P2b</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Khl</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zadan� pr�ce do R</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tal</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4ag</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PF</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pi</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4ag, 4bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEF<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Zastupov�n�-p�tek  21. 9. 2018</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Refresh" content="180">
<STYLE TYPE="text/css"><!--
.StyleB0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleB1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleB2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,204)}

.StyleC0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleC1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleC2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC3 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC4 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC5 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC6 { border-style: solid;padding:0px; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleH2 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleZ0 { background-color: #DBE6EE }

.StyleZ1 { width: 700}

.StyleZ2 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: right; width: 700}

.StyleZ3 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ4 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ5 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: left}

.StyleZ6 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: right}

--></STYLE>
</HEAD>
<BODY CLASS="StyleZ0">
<DIV CLASS="StyleZ2">Gymn�zium Brno, t��da Kapit�na Jaro�e, p��sp�vkov� organizace</DIV>
<DIV CLASS="StyleZ3">p�tek  21. 9. 2018</DIV><BR>
<DIV CLASS="StyleZ4">Chyb�j�c�</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">Herman, Clausov�, Kallus Brychov�, Kobza, Liter�kov� (0..3), Ma��kov�, Pulkr�bek (4), Stupka, Zub��kov�</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">4bg</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Suplov�n�</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="98" HEIGHT="0"></TD>
<TD WIDTH="41" HEIGHT="0"></TD>
<TD WIDTH="38" HEIGHT="0"></TD>
<TD WIDTH="49" HEIGHT="0"></TD>
<TD WIDTH="47" HEIGHT="0"></TD>
<TD WIDTH="43" HEIGHT="0"></TD>
<TD WIDTH="90" HEIGHT="0"></TD>
<TD WIDTH="74" HEIGHT="0"></TD>
<TD WIDTH="25" HEIGHT="0"></TD>
<TD WIDTH="160" HEIGHT="0"></TD>
<TD WIDTH="35" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">T��da</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Budova</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Hodina</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Chyb�j�c�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">P�edm�t</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">U�ebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">N�hradn� u�ebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zastupuj�c�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Typ zastupov�n�</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Pozn�mka pro vyu�uj�c�ho</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zna�ka</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">He</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kre</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=3 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lac</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">CvMn</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">7</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">CvMn</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hor</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S, D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hor</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S, D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.B, 2.C, 2.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pul</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Du3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Vv</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">La3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ju</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ple</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hnd</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=3 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Z</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mrk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ma�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mrk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=4 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Hk</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">In</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mcv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kob</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mcv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.B</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.B, 4.C, 4.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">La3L</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4.A, 4.B, 4.C, 4.D</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Stu</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sq</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">7</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Stu</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tv</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sq</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P1b</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pt</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P2b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sm�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3ag</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3a</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sta</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zadan� pr�ce do A</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=2 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3ag, 3bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3a</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Ad</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">5</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">R</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P2b</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Khl</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zadan� pr�ce do R</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">6</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kbr</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Tal</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4ag</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Zub</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P3b</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PF</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Pi</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4ag, 4bg</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">P</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sm�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">film</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Ozn�men�</DIV>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleH2" HEIGHT="0px">Jaro�ka i P���n�:<BR>V 9.00 hod. je posledn� term�n nahl�en� p��padn�ch zm�n v rozvrhu v�etn� u�eben Z� Bc.<BR><BR>P���n�:<BR>Jak se u�it - beseda s psycholo�kou pro 1.ag (1. hod., dozor Ku�) a 1.bg (2. hod., dozor Khl) <BR>4ag, 4bg maj� 6. hod. spojenou Vv (p��prava divadla) . <BR>J�delna:<BR>13.50 - 14.10   Tal</TD>
</TR>
</TABLE>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE WIDTH=700 BORDER=0><TR><TD CLASS="StyleZ5">20.9.2018  11:29</TD><TD CLASS="StyleZ6" ALIGN=RIGHT>generovan� programem Zastupovanie 5 � RNDr.�erven� &nbsp;&nbsp;<A HREF="http://www.cerveny.sk">www.cerveny.sk</A></TD></TR></TABLE>
<BR><BR><BR>
</BODY>
</HTML>
T" CLASS="StyleC2" HEIGHT="0px">Cla</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">N</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">PJ</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Sm�</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">film</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Ozn�men�</DIV>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleH2" HEIGHT="0px">Jaro�ka i P���n�:<BR>V 9.00 hod. je posledn� term�n nahl�en� p��padn�ch zm�n v rozvrhu v�etn� u�eben Z� Bc.<BR><BR>P���n�:<BR>Jak se u�it - beseda s psycholo�kou pro 1.ag (1. hod., dozor Ku�) a 1.bg (2. hod., dozor Khl) <BR>4ag, 4bg maj� 6. hod. spojenou Vv (p��prava divadla) . <BR>J�delna:<BR>13.50 - 14.10   Tal</TD>
</TR>
</TABLE>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE WIDTH=700 BORDER=0><TR><TD CLASS="StyleZ5">20.9.2018  11:29</TD><TD CLASS="StyleZ6" ALIGN=RIGHT>generovan� programem Zastupovanie 5 � RNDr.�erven� &nbsp;&nbsp;<A HREF="http://www.cerveny.sk">www.cerveny.sk</A></TD></TR></TABLE>
<BR><BR><BR>
</BODY>
</HTML>
`
