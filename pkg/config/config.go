package config

import (
	"encoding/json"
	"os"

	"github.com/Sirupsen/logrus"
)

//Config ... main config struct
type Config struct {
	ID      int    `json:"supl_id"`
	Token   string `json:"bot_token"`
	SuplURL string `json:"supl_url"`
	file    string
	log     *logrus.Logger
	Banned  []int `json:"banned"`
}

//InitConf ... initialize config
func InitConf(file string, log *logrus.Logger) *Config {
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0664)
	defer f.Close()
	if err != nil {
		log.WithField("name", file).Fatalf("Can't open config file: '%s'", err.Error())
	}
	c := &Config{file: file, log: log}
	return c
}

//SaveConfig ... saves config to cfg file
func (c *Config) SaveConfig() {
	f, err := os.OpenFile(c.file, os.O_RDWR, 0664)
	defer f.Close()
	if err != nil {
		c.log.WithField("name", c.file).Errorf("Can't open config file: '%s'", err.Error())
	}
	j, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		c.log.WithError(err).Errorf("Can't marshal config: '%s'", err.Error())
	}
	_, err = f.Write(j)
	if err != nil {
		c.log.WithError(err).Errorf("Can't write config: '%s'", err.Error())
	}
}

//LoadConfig ... loads config from cfg file
func (c *Config) LoadConfig() {
	f, err := os.OpenFile(c.file, os.O_RDONLY, 0664)
	defer f.Close()
	if err != nil {
		c.log.WithField("name", c.file).Errorf("Can't open config file: '%s'", err.Error())
	}
	err = json.NewDecoder(f).Decode(c)
	if err != nil {
		c.log.WithError(err).Errorf("Can't unmarshal config: '%s'", err.Error())
	}
}

//IsBanned ... return true if user is banned in config
func (c *Config) IsBanned(usr int) bool {
	for _, v := range c.Banned {
		if v == usr {
			return true
		}
	}
	return false
}
