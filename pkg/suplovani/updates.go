package suplovani

import (
	"fmt"
	"unicode/utf8"

	"bitbucket.org/olik636/suplbot/pkg/structs"
	"github.com/Sirupsen/logrus"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Controller) sendUpdates(up map[string][]structs.SuplItem, req map[string][]request, day string) {
	var chats map[int]bool
	var rmsg string
	if len(up) > 0 && len(req) > 0 {
		c.Log.WithFields(logrus.Fields{"updates": up, "requests": req}).Debugf("Sending updates")
	}
	for i, v := range req {
		for _, u := range v {
			_, ok := chats[u.ChatID]
			if up[i] == nil || !ok {
				continue
			}
			rmsg = "Změna " + day + "ho suplování! Nově:\n" + GenerateCombinedSupl("", up[i])

			msg := tgbotapi.NewMessage(int64(u.ChatID), rmsg)
			if u.MessageID != 0 {
				msg.ReplyToMessageID = u.MessageID
			}
			msg.ParseMode = "markdown"
			_, err := c.Bot.Send(msg)
			if err != nil {
				c.Log.WithError(err).Errorf("Can't send message")
			}
			chats[u.ChatID] = true
		}
	}
}

//GenerateCombinedSupl ... generated  supl text from multiple SuplItems
func GenerateCombinedSupl(msg string, o []structs.SuplItem) string {
	for _, v := range o {
		msg += fmt.Sprintf("*%s. hodina*\n``` %s: %s\n %s: %s\n %s: %s\n",
			v.Hour, padding("Chybí", paddingSize), v.Missing, padding("Předmět", paddingSize),
			v.Subject, padding("Místnost", paddingSize), v.Classroom)
		if v.NewClassroom != "" {
			msg += fmt.Sprintf(" %s: %s\n", padding("Náhrada", paddingSize), v.NewClassroom)
		}
		if v.SupplyTeacher != "" {
			msg += fmt.Sprintf(" %s: %s\n", padding("Zastupuje", paddingSize), v.SupplyTeacher)
		}
		if v.Type != "" {
			msg += fmt.Sprintf(" %s: %s\n", padding("Typ", paddingSize), v.Type)
		}
		if v.Note != "" {
			msg += fmt.Sprintf(" %s: %s\n", padding("Poznámka", paddingSize), v.Note)
		}
		msg += "```"
	}
	return msg
}

func padding(s string, n int) string {
	for l := utf8.RuneCountInString(s); l < n; l = utf8.RuneCountInString(s) {
		s += " "
	}
	return s
}
