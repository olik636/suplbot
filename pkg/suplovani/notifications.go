package suplovani

import (
	"fmt"

	"bitbucket.org/olik636/suplbot/pkg/database"
	"bitbucket.org/olik636/suplbot/pkg/structs"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func (supl *Suplovani) sendNotifications(c *Controller, req []map[string][]request) {
	chats, err := database.SelectAllWithNotification(c.DbH)
	var umsg tgbotapi.MessageConfig
	if err != nil {
		c.Log.WithError(err).Error("Can't get users with notifications turned on")
	}
	msg := fmt.Sprintf("*Dobré ráno!*\nDnešní [suplování](%s) pro %%s:\n%%s", supl.TodayURL)
	for _, v := range chats {
		class, err := database.GetClass(c.DbH, int(v))
		if err != nil {
			c.Log.WithError(err).Error("Can't get user's class")
		}
		if class == 0 {
			umsg = tgbotapi.NewMessage(int64(v), fmt.Sprintf("*Dobré ráno!*\nDnešní [suplování](%s)", supl.TodayURL))
		} else {
			registerRequest(SuplRequest{MessageID: 0, ChatID: int(v),
				Class: structs.ClassString(class), Day: TODAY}, req)
			umsg = tgbotapi.NewMessage(int64(v), fmt.Sprintf(msg, structs.ClassString(class),
				GenerateCombinedSupl("", supl.Today[structs.ClassString(class)])))
		}
		umsg.ParseMode = "markdown"
		c.Bot.Send(umsg)
	}
}
