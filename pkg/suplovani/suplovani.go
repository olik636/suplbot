package suplovani

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/olik636/suplbot/pkg/config"
	"bitbucket.org/olik636/suplbot/pkg/parse"
	"bitbucket.org/olik636/suplbot/pkg/structs"
	"github.com/Sirupsen/logrus"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"golang.org/x/text/encoding/charmap"
)

const schoolNum = 0

//Today ... today const
const (
	TODAY = iota
	TOMORROW
)

// Controller commands
const (
	CMDHolidayToggle = iota
	CMDNextID
	CMDPreviusID
)

const paddingSize = 10
const updateInterval = 5 * time.Minute

//Suplovani ... supl
type Suplovani struct {
	ID          int
	TodayURL    string
	TomorrowURL string
	IsTomorrow  bool
	Today       map[string][]structs.SuplItem
	Tomorrow    map[string][]structs.SuplItem
	TodayDay    time.Weekday
	TomorrowDay time.Weekday
	IsHoliday   bool
}

//Controller ... main controller object
type Controller struct {
	ID  int
	URL *url.URL
	DbH *sql.DB
	Log *logrus.Logger
	Bot *tgbotapi.BotAPI
	Cfg *config.Config
}

//SuplRequest ... supl request from commands
type SuplRequest struct {
	ChatID    int
	MessageID int
	Class     string
	Day       int
	Chan      chan Suplovani
}

type request struct {
	ChatID    int
	MessageID int
}

//Init ... init controller
func Init(start int, rawurl string, db *sql.DB, log *logrus.Logger, bot *tgbotapi.BotAPI, cfg *config.Config) (*Controller, error) {
	var err error
	c := Controller{ID: start, DbH: db, Log: log, Bot: bot, Cfg: cfg}
	c.URL, err = url.Parse(rawurl)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

//Run ... core of the controller
func (c *Controller) Run(suplChan chan SuplRequest, cmds chan uint) {
	supl := &Suplovani{ID: c.ID}
	var last = time.Now().Add(-1 * updateInterval)
	var requests = make([]map[string][]request, 2)
	var notified, update = true, true
	requests[TODAY] = map[string][]request{}
	requests[TOMORROW] = map[string][]request{}
	var err error
	err = updateID(supl, c.URL)
	if err != nil {
		c.Log.WithError(err).Errorf("Can't update ID's")
	}

	for {
		select {
		case cmd := <-cmds:
			switch cmd {
			case CMDHolidayToggle:
				supl.IsHoliday = !supl.IsHoliday
			case CMDNextID:
				supl.ID++
				requests[TODAY] = requests[TOMORROW]
				requests[TOMORROW] = map[string][]request{}
				updateID(supl, c.URL)
				update = true
			case CMDPreviusID:
				supl.ID--
				requests[TODAY] = requests[TOMORROW]
				requests[TOMORROW] = map[string][]request{}
				updateID(supl, c.URL)
				update = true
			}
		case req := <-suplChan:
			req.Chan <- *supl
			registerRequest(req, requests)
		default:
			if last.Add(updateInterval).Before(time.Now()) || update {
				c.Cfg.ID = supl.ID
				c.Cfg.SaveConfig()
				supl.update(c, last, requests, &notified)
				last = time.Now()
				if time.Now().Hour() > 4 && !notified {
					supl.sendNotifications(c, requests)
					notified = true
				}
				update = false
			}
		}
	}
}

//update updates supl
func (supl *Suplovani) update(c *Controller, lastUpdate time.Time, requests []map[string][]request, notified *bool) {
	if lastUpdate.Day() != time.Now().Day() && !supl.IsHoliday {
		if time.Now().Weekday() != time.Sunday && time.Now().Weekday() != time.Saturday {
			supl.ID++
			*notified = false
			c.Log.WithField("id", supl.ID).Debug("Updated supl ID")
			requests[TODAY] = requests[TOMORROW]
			requests[TOMORROW] = map[string][]request{}
			updateID(supl, c.URL)
		}
	}
	updates, err := supl.updateData()
	if len(updates[TODAY]) != 0 || len(updates[TOMORROW]) != 0 {
		c.Log.WithFields(logrus.Fields{"today": updates[TODAY], "tomorrow": updates[TOMORROW]}).Debug("Updates found")
	}
	d, _ := structs.GenerateDay(supl.TodayDay, c.Log)
	td, _ := structs.GenerateDay(supl.TomorrowDay, c.Log)
	c.sendUpdates(updates[TODAY], requests[TODAY], d)
	c.sendUpdates(updates[TOMORROW], requests[TOMORROW], td)
	if err != nil {
		c.Log.WithError(err).Error("Can't update supl")
	}
}

//register supl request
func registerRequest(req SuplRequest, requests []map[string][]request) {
	if req.Class != "" {
		_, ok := requests[req.Day][req.Class]
		if !ok {
			requests[req.Day][req.Class] = []request{}
		}
		requests[req.Day][req.Class] = append(requests[req.Day][req.Class], request{MessageID: req.MessageID, ChatID: req.ChatID})
	}
}

// updates supl data + returns updated classes
func (supl *Suplovani) updateData() ([2]map[string][]structs.SuplItem, error) {
	var updates [2]map[string][]structs.SuplItem
	var err error
	supl.Today, updates[TODAY], err = updateSupl(supl.TodayURL, supl.Today)
	supl.Tomorrow, updates[TOMORROW], err = updateSupl(supl.TomorrowURL, supl.Tomorrow)
	supl.IsTomorrow = supl.Tomorrow != nil

	return updates, err
}

func updateSupl(url string, old map[string][]structs.SuplItem) (map[string][]structs.SuplItem, map[string][]structs.SuplItem, error) {
	var updates = map[string][]structs.SuplItem{}
	resp, err := http.Get(url)
	if err != nil {
		return old, nil, fmt.Errorf("Can't get supl: '%s'", err.Error())
	}
	defer resp.Body.Close()
	d := charmap.Windows1250.NewDecoder()
	wdata, err := ioutil.ReadAll(resp.Body)
	data, err := d.Bytes(wdata)
	if err != nil {
		return old, nil, err
	}
	if strings.TrimSpace(string(data)) == "" {
		return nil, nil, nil
	}
	s, err := parse.HTML(string(data))
	if err != nil {
		return old, nil, err
	}
	if old != nil {
		var found bool
		for i, v := range s {
			if !reflect.DeepEqual(v, old[i]) {
				for _, fv := range v {
					for _, ofv := range old[i] {
						if reflect.DeepEqual(fv, ofv) {
							found = true
						}
					}
					if !found {
						_, ok := updates[i]
						if !ok {
							updates[i] = []structs.SuplItem{}
						}
						updates[i] = append(updates[fv.Classroom], fv)
					}
				}
			}
		}
	}
	return s, updates, nil
}

func updateID(supl *Suplovani, surl *url.URL) error {
	var err error
	supl.TodayURL = encodeURL(supl.ID, *surl)
	supl.TomorrowURL = encodeURL(supl.ID+1, *surl)

	if time.Now().Add(24*time.Hour).Weekday() == time.Saturday || time.Now().Add(24*time.Hour).Weekday() == time.Sunday {
		supl.TomorrowDay = time.Monday
	} else {
		supl.TomorrowDay = time.Now().Add(24 * time.Hour).Weekday()
	}
	supl.TodayDay = time.Now().Weekday()

	return err
}

func encodeURL(id int, surl url.URL) string {
	val := url.Values{}
	val.Add("skola", strconv.Itoa(schoolNum))
	val.Add("id", strconv.Itoa(id))
	surl.RawQuery = val.Encode()
	return surl.String()
}
