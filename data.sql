CREATE TABLE users(
        name varchar(64) not null,
        chat_id varchar(64) not null, 
        dt_registration datetime not null, 
        notify boolean not null default 0,
        class unsigned tinyint default 0
); 
