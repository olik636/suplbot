package main

import (
	"flag"
	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"golang.org/x/text/encoding/charmap"
)

var log = logrus.New()
var up bool

func main() {
	var addr string
	flag.StringVar(&addr, "p", "localhost:5000", "Host + port bind")
	flag.BoolVar(&up, "u", false, "Updated supl")
	flag.Parse()

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/supl", getSupl)
	if err := http.ListenAndServe(addr, r); err != nil {
		log.WithError(err).Fatal("ListenAndServe failed")
	}
}

func getSupl(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["id"]
	var un []byte
	if !ok || len(keys[0]) < 1 {
		log.Warn("Get without  key \"id\"")
	}
	w.Header().Add("Content-Type", "text/html; charset=windows-1250")
	if up {
		un = []byte(suplUp)
	} else {
		un = []byte(supl)
	}
	dec := charmap.Windows1250.NewEncoder()
	d, _ := dec.Bytes(un)
	w.Write(d)
}

var supl = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Zastupování-pátek  21. 9. 2018</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Refresh" content="180">
<STYLE TYPE="text/css"><!--
.StyleB0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleB1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleB2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,204)}

.StyleC0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleC1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleC2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC3 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC4 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC5 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC6 { border-style: solid;padding:0px; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleH2 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleZ0 { background-color: #DBE6EE }

.StyleZ1 { width: 700}

.StyleZ2 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: right; width: 700}

.StyleZ3 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ4 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ5 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: left}

.StyleZ6 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: right}

--></STYLE>
</HEAD>
<BODY CLASS="StyleZ0">
<DIV CLASS="StyleZ2">Gymnázium Brno, třída Kapitána Jaroše, příspěvková organizace</DIV>
<DIV CLASS="StyleZ3">pátek  21. 9. 2018</DIV><BR>
<DIV CLASS="StyleZ4">Chybějící</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">Herman, Clausová, Kallus Brychová, Kobza, Literáková (0..3), Maříková, Pulkrábek (4), Starcová, Stupka, Zubíčková</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">4bg</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Suplování</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="98" HEIGHT="0"></TD>
<TD WIDTH="41" HEIGHT="0"></TD>
<TD WIDTH="38" HEIGHT="0"></TD>
<TD WIDTH="49" HEIGHT="0"></TD>
<TD WIDTH="47" HEIGHT="0"></TD>
<TD WIDTH="43" HEIGHT="0"></TD>
<TD WIDTH="90" HEIGHT="0"></TD>
<TD WIDTH="74" HEIGHT="0"></TD>
<TD WIDTH="25" HEIGHT="0"></TD>
<TD WIDTH="160" HEIGHT="0"></TD>
<TD WIDTH="35" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Třída</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Budova</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Hodina</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Chybějící</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Předmět</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Učebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Náhradní učebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zastupující</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Typ zastupování</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Poznámka pro vyučujícího</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Značka</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mař</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kre</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Č</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lac</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Oznámení</DIV>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleH2" HEIGHT="0px">Jaroška i Příční:<BR>V 9.00 hod. je poslední termín nahlášení případných změn v rozvrhu včetně učeben ZŘ Bc.<BR><BR>Příční:<BR>Jak se učit - beseda s psycholožkou pro 1.ag (1. hod., dozor Kuč) a 1.bg (2. hod., dozor Khl) <BR>4ag, 4bg mají 6. hod. spojenou Vv (příprava divadla) . <BR>Jídelna:<BR>13.50 - 14.10   Tal</TD>
</TR>
</TABLE>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE WIDTH=700 BORDER=0><TR><TD CLASS="StyleZ5">21.9.2018  7:50</TD><TD CLASS="StyleZ6" ALIGN=RIGHT>generované programem Zastupovanie 5 © RNDr.Červený &nbsp;&nbsp;<A HREF="http://www.cerveny.sk">www.cerveny.sk</A></TD></TR></TABLE>
<BR><BR><BR>
</BODY>
</HTML>
`

//with 4.ag, Zub
var suplUp = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Zastupování-pátek  21. 9. 2018</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1250">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Refresh" content="180">
<STYLE TYPE="text/css"><!--
.StyleB0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleB1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleB2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,204)}

.StyleC0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleC1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleC2 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC3 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC4 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC5 { border-style: solid;padding:0px; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleC6 { border-style: solid;padding:0px; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 1pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH0 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(109,109,109); background-color: rgb(255,255,255)}

.StyleH1 { border-style: solid; padding:3; border-left-width: 1; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-color: rgb(160,160,160); border-top-color: rgb(160,160,160); border-right-color: rgb(160,160,160); border-bottom-color: rgb(160,160,160); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(240,240,240)}

.StyleH2 { border-style: solid; padding:3; border-left-width: 0; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-color: rgb(0,0,0); border-top-color: rgb(0,0,0); border-right-color: rgb(0,0,0); border-bottom-color: rgb(0,0,0); font-family: 'Tahoma'; mso-font-charset: 1; font-size: 8pt; color: rgb(0,0,0); background-color: rgb(255,255,255)}

.StyleZ0 { background-color: #DBE6EE }

.StyleZ1 { width: 700}

.StyleZ2 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: right; width: 700}

.StyleZ3 { color: #4C85AB; font-family: Tahoma; font-size: 10pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ4 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: bold; font-style: normal; text-align: left}

.StyleZ5 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: left}

.StyleZ6 { color: #4C85AB; font-family: Tahoma; font-size: 8pt; font-weight: normal; font-style: normal; text-align: right}

--></STYLE>
</HEAD>
<BODY CLASS="StyleZ0">
<DIV CLASS="StyleZ2">Gymnázium Brno, třída Kapitána Jaroše, příspěvková organizace</DIV>
<DIV CLASS="StyleZ3">pátek  21. 9. 2018</DIV><BR>
<DIV CLASS="StyleZ4">Chybějící</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">Herman, Clausová, Kallus Brychová, Kobza, Literáková (0..3), Maříková, Pulkrábek (4), Starcová, Stupka, Zubíčková</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleB2" HEIGHT="0px">4bg</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Suplování</DIV>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="98" HEIGHT="0"></TD>
<TD WIDTH="41" HEIGHT="0"></TD>
<TD WIDTH="38" HEIGHT="0"></TD>
<TD WIDTH="49" HEIGHT="0"></TD>
<TD WIDTH="47" HEIGHT="0"></TD>
<TD WIDTH="43" HEIGHT="0"></TD>
<TD WIDTH="90" HEIGHT="0"></TD>
<TD WIDTH="74" HEIGHT="0"></TD>
<TD WIDTH="25" HEIGHT="0"></TD>
<TD WIDTH="160" HEIGHT="0"></TD>
<TD WIDTH="35" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Třída</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Budova</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Hodina</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Chybějící</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Předmět</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Učebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Náhradní učebna</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Zastupující</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Typ zastupování</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Poznámka pro vyučujícího</TD>
<TD ALIGN="LEFT" CLASS="StyleC1" HEIGHT="0px">Značka</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">He</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">M</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1A</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">---</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1.C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Mař</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">1C</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Kre</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">S</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
<TR>
<TD ROWSPAN=3 ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">2.A</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">J</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">3</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lit</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Č</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">4B</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">Lac</TD>
<TD ALIGN="LEFT" CLASS="StyleC2" HEIGHT="0px">D</TD>
<TD ALIGN="LEFT" CLASS="StyleC4" HEIGHT="0px">&nbsp</TD>
<TD ALIGN="CENTER" CLASS="StyleC5" HEIGHT="0px">&nbsp</TD>
</TR>
</TABLE>
<BR><BR>
<DIV CLASS="StyleZ4">Oznámení</DIV>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE BORDER="0" CELLSPACING="0" WIDTH="700">
<TR>
<TD WIDTH="700" HEIGHT="0"></TD>
</TR>
<TR>
<TD ALIGN="LEFT" CLASS="StyleH2" HEIGHT="0px">Jaroška i Příční:<BR>V 9.00 hod. je poslední termín nahlášení případných změn v rozvrhu včetně učeben ZŘ Bc.<BR><BR>Příční:<BR>Jak se učit - beseda s psycholožkou pro 1.ag (1. hod., dozor Kuč) a 1.bg (2. hod., dozor Khl) <BR>4ag, 4bg mají 6. hod. spojenou Vv (příprava divadla) . <BR>Jídelna:<BR>13.50 - 14.10   Tal</TD>
</TR>
</TABLE>
<HR CLASS="StyleZ4" WIDTH=700 ALIGN=LEFT>
<TABLE WIDTH=700 BORDER=0><TR><TD CLASS="StyleZ5">21.9.2018  7:50</TD><TD CLASS="StyleZ6" ALIGN=RIGHT>generované programem Zastupovanie 5 © RNDr.Červený &nbsp;&nbsp;<A HREF="http://www.cerveny.sk">www.cerveny.sk</A></TD></TR></TABLE>
<BR><BR><BR>
</BODY>
</HTML>

`
