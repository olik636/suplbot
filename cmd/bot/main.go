package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/olik636/suplbot/pkg/commands"
	"bitbucket.org/olik636/suplbot/pkg/config"
	"bitbucket.org/olik636/suplbot/pkg/database"
	"bitbucket.org/olik636/suplbot/pkg/suplovani"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	_ "github.com/mattn/go-sqlite3"
)

type command struct {
	F        func(c *commands.Context)
	AdminCMD bool
	Help     string
}

var cmds = map[string]command{
	"dnes":       command{F: commands.Dnes, Help: "zobrazí dnešní suplování"},
	"zitra":      command{F: commands.Zitra, Help: "zobrazí zítřejší suplování"},
	"trida":      command{F: commands.SetClass, Help: "nastaví třídu `/trida 1.A`"},
	"notifikace": command{F: commands.ToggleNotifications, Help: "zapíná ranní notifikace"},
	"holiday":    command{F: commands.ToggleHoliday, AdminCMD: true},
	"broadcast":  command{F: commands.Broadcast, AdminCMD: true},
	"list":       command{F: commands.List, AdminCMD: true},
	"previd":     command{F: commands.PreviousID, AdminCMD: true},
	"nextid":     command{F: commands.NextID, AdminCMD: true},
}

func main() {
	var debug bool
	flag.BoolVar(&debug, "debug", false, "Debug output")
	flag.Parse()
	log := logrus.New()
	if debug {
		log.SetLevel(logrus.DebugLevel)
	}
	log.Formatter = &logrus.TextFormatter{ForceColors: true}
	cfg := config.InitConf("cfg.json", log)
	cfg.LoadConfig()

	//Otevreni a nastaveni logu
	logFileName, err := os.OpenFile("bot.log", os.O_CREATE|os.O_RDWR|os.O_APPEND, 999)
	if err != nil {
		log.Fatalf("Error while opening log file: '%s'", err.Error())
	}
	log.Out = io.MultiWriter(logFileName, os.Stdout)

	//Pripojeni k database
	dbh, err := database.Connect("data.db")
	if err != nil {
		log.Fatalf("Error while connecting to the database: '%s'", err.Error())
	}

	//Pripojeni k bot api
	bot, err := tgbotapi.NewBotAPI(cfg.Token)
	if err != nil {
		log.Fatalf("Error while connecting to the bot api: '%s'", err.Error())
	}

	log.Infof("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	s, err := suplovani.Init(cfg.ID, cfg.SuplURL, dbh, log, bot, cfg)
	if err != nil {
		log.WithError(err).Panicf("Can't initialize supl")
	}

	slc := make(chan suplovani.SuplRequest)
	cmc := make(chan uint)
	go s.Run(slc, cmc)

	ctx := &commands.Context{Dbh: dbh, Log: log, Bot: bot, Supl: slc, CMDs: cmc}

	updates, err := bot.GetUpdatesChan(u)
	for update := range updates {

		if update.Message == nil {
			continue
		}

		if cfg.IsBanned(int(update.Message.Chat.ID)) {
			log.Warnf("Blocked: {%d}[%s %s] %s", update.Message.Chat.ID, update.Message.From.FirstName,
				update.Message.From.LastName, update.Message.Text)
			continue
		}

		log.Infof("{%d}[%s %s] %s", update.Message.Chat.ID, update.Message.From.FirstName,
			update.Message.From.LastName, update.Message.Text)

		_, err := database.RegisterUser(dbh, int(update.Message.Chat.ID),
			fmt.Sprintf("%s %s", update.Message.Chat.FirstName, update.Message.Chat.LastName))
		if err != nil {
			log.Errorf("Error while logging in user: '%s'", err.Error())
		}

		if update.Message.Command() == "help" {
			help(update, bot)
			continue
		}

		c, ok := cmds[update.Message.Command()]
		if !ok || c.AdminCMD && !isAdmin(update.Message.From.UserName) {
			continue
		}
		ctx.Update = &update
		c.F(ctx)
	}
}

func isAdmin(user string) bool {
	return user == "olik636" || user == "OK5TOM"
}

func help(update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	var rmsg = "*Příkazy*\n"
	for i, v := range cmds {
		if !v.AdminCMD {
			rmsg += "` " + i + "` - " + v.Help + "\n"
		}
	}
	rmsg += "\nJakékoliv náměty, či připomínky vítám @olik636"
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, rmsg)
	msg.ReplyToMessageID = update.Message.MessageID
	msg.ParseMode = "markdown"
	bot.Send(msg)

}
